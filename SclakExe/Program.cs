﻿using com.sclak.sclak;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SclakExe
{
    class Program
    {
		private const String CMD_VER = "-v";
		private const String CMD_TIME = "-time";
        private const String CMD_GET_TOKEN = "gt";
        private const String CMD_GET_BT_CODE = "gb";
        private const String CMD_UPDATE_PERIPHERAL = "up";
		private const String CMD_UPDATE_PERIPHERAL_VERSION = "upv";
        private const String CMD_GET_SECRET = "gs";

        static void Main(string[] args)
        {
            SclakDLL sclakDLL = new SclakDLL();

			string cmd = null;
			if (args.Length == 0)
			{
				//cmd = CMD_TIME;
				return;
			}
			else
			{
				cmd = args[0];
			}

            WrapperResponse res = null;
            switch (cmd) {
			case CMD_VER:
				Console.WriteLine (String.Format ("SclakExe version " + Constants.VERSION + " - {0}", Constants.BASE_URL));
				break;

			case CMD_TIME:
				res = sclakDLL.getTime();
                break;

			case CMD_GET_TOKEN:
				if (args.Length < 3) {
					Console.WriteLine (String.Format ("{0}|{1}|{2}", "99", "Invalid parameters for " + CMD_GET_TOKEN, ""));
					return;
				}
				res = sclakDLL.getToken (args [1], args [2]);
				break;

			case CMD_GET_BT_CODE:
				if (args.Length < 4) {
					Console.WriteLine (String.Format ("{0}|{1}|{2}", "99", "Invalid parameters for " + CMD_GET_BT_CODE, ""));
					return;
				}
				res = sclakDLL.getAvailableBtCode (args [1], args [2], args[3]);
				break;			

			case CMD_GET_SECRET:
				if (args.Length < 3) {
					Console.WriteLine (String.Format ("{0}|{1}|{2}", "99", "Invalid parameters for " + CMD_GET_SECRET, ""));
					return;
				}
				res = sclakDLL.getPeripheralSecretWithBtCode (args [1], args [2]);
				break;

			case CMD_UPDATE_PERIPHERAL:
				if (args.Length < 6 || args.Length > 7)
				{
					Console.WriteLine(String.Format("{0}|{1}|{2}", "99", "Invalid parameters for " + CMD_UPDATE_PERIPHERAL, ""));						return;
				}
				if (args.Length == 6)
					res = sclakDLL.updatePeripheral(args[1], args[2], args[3], args[4], args[5], null);
				else if (args.Length == 7)
					res = sclakDLL.updatePeripheral(args[1], args[2], args[3], args[4], args[5], args[6]);
                break;

			case CMD_UPDATE_PERIPHERAL_VERSION:
		    	if (args.Length < 4 || args.Length > 4) {
					Console.WriteLine (String.Format ("{0}|{1}|{2}", "99", "Invalid parameters for " + CMD_UPDATE_PERIPHERAL_VERSION, ""));
					return;
				}
				res = sclakDLL.updatePeripheralVersion (args [1], args [2], args [3]);
				break;
			}

            printResponse(res);
        }

        private static void printResponse(WrapperResponse res)
        {
            if (res != null)
            {
                Console.WriteLine(String.Format("{0}|{1}|{2}", res.errorCode, res.errorMessage, res.result));
            }
        }
    }
}
