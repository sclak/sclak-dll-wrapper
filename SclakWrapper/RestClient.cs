﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Net.Security;

namespace com.sclak.sclak
{
    public enum HttpVerb
    {
        GET,
        POST,
        PUT,
        DELETE
    }


    class RestClient
    {
        public string EndPoint { get; set; }
        public HttpVerb Method { get; set; }
        public string ContentType { get; set; }
        public string PostData { get; set; }
        public string AuthHeader { get; set; }

        public RestClient()
        {
            EndPoint = "";
            Method = HttpVerb.GET;
            PostData = "";
            AuthHeader = "";
        }
        public RestClient(string endpoint)
        {
            EndPoint = endpoint;
            Method = HttpVerb.GET;
            PostData = "";
            AuthHeader = "";
        }
        public RestClient(string endpoint, HttpVerb method)
        {
            EndPoint = endpoint;
            Method = method;
            PostData = "";
            AuthHeader = "";
        }

        public RestClient(string endpoint, HttpVerb method, string postData, string authHeader)
        {
            EndPoint = endpoint;
            Method = method;
            PostData = postData;
            AuthHeader = authHeader;
        }


        public string MakeRequest()
        {
            return MakeRequest("");
        }
        
        public string MakeRequest(string parameters)
        {            

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var request = (HttpWebRequest) WebRequest.Create(EndPoint + parameters);            

            request.Method = Method.ToString();
            request.ContentLength = 0;
			request.ContentType = "application/json";
            request.UserAgent = "SclakTestingBox/" + Constants.VERSION;            

            if (!string.IsNullOrEmpty(AuthHeader))
            {
                request.Headers.Add("AUTHORIZATION", "token " + AuthHeader);
            }

            if (!string.IsNullOrEmpty(PostData) && (Method == HttpVerb.POST || Method == HttpVerb.PUT))
            {
                // default encoding UTF8
                var bytes = Encoding.UTF8.GetBytes(PostData);

                // try legacy encoding if UTF8 fails
                if (0 == bytes.Length)                    
                    bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(PostData);

                request.ContentLength = bytes.Length;

                using (var writeStream = request.GetRequestStream())
                {
                    writeStream.Write(bytes, 0, bytes.Length);
                }
            }

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                var responseValue = string.Empty;

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    var message = String.Format("Request failed. Received HTTP {0}", response.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response
                using (var responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                }

                return responseValue;
            }
        }
    }
}
