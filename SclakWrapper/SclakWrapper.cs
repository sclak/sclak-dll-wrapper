﻿using System;
using System.IO;
using System.Collections;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Collections.Specialized;
using System.Linq;

namespace com.sclak.sclak
{    
    public class SclakWrapper
    {
        string currToken, currPeripheralId;

        public WrapperResponse getToken(string user, string password)
        {
            TokenResponse jsonResponse = null;

            try
            {
                var client = new RestClient();
                client.EndPoint = Constants.BASE_URL + Constants.TOKEN_URL;
                client.Method = HttpVerb.GET;

                NameValueCollection parms = new NameValueCollection();
                parms.Add(Constants.EMAIL_PARAM, user);
                parms.Add(Constants.PASSWORD_PARAM, password);
                var json = client.MakeRequest(CommonUtilities.toQueryString(parms));
                Console.WriteLine(json);

                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(TokenResponse));
                object objResponse = jsonSerializer.ReadObject(CommonUtilities.convertStringToStream(json));
                jsonResponse = objResponse as TokenResponse;
                ProcessResponse(jsonResponse);

                currToken = jsonResponse.Token;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            WrapperResponse res = new WrapperResponse();
            if (jsonResponse != null)
            {
                res.errorCode = jsonResponse.ErrorCode;
                res.errorMessage = jsonResponse.ErrorMessage;
                res.result = currToken;
            }

            return res;
        }

        public WrapperResponse getBtCode(string token, string lot)
        {
            BtcodeResponse jsonResponse = null;

            try
            {
                var client = new RestClient();
                client.EndPoint = Constants.BASE_URL + Constants.BTCODE_URL;
                client.Method = HttpVerb.GET;
                client.AuthHeader = token;

                NameValueCollection parms = new NameValueCollection();
				parms.Add(Constants.FW_CODE_PARAM, lot);
                var json = client.MakeRequest(CommonUtilities.toQueryString(parms));
                Console.WriteLine(json);

                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(BtcodeResponse));
                object objResponse = jsonSerializer.ReadObject(CommonUtilities.convertStringToStream(json));
                jsonResponse = objResponse as BtcodeResponse;
                ProcessResponse(jsonResponse);

                currPeripheralId = jsonResponse.Id;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            WrapperResponse res = new WrapperResponse();
            if (jsonResponse != null)
            {
                res.errorCode = jsonResponse.ErrorCode;
                res.errorMessage = jsonResponse.ErrorMessage;
                res.result = jsonResponse.Btcode;
            }

            return res;
        }

        public WrapperResponse updatePeripheral(string token, string secret)
        {
            ResponseObject jsonResponse = null;

            try
            {
                var client = new RestClient();
                client.EndPoint = Constants.BASE_URL + Constants.PERIPHERALS_URL + currPeripheralId;
                client.Method = HttpVerb.PUT;
                client.AuthHeader = token;

                NameValueCollection parms = new NameValueCollection();
                parms.Add(Constants.SECRET_PARAM, secret);
                parms.Add(Constants.STATUS_PARAM, "3");
                client.PostData = Constants.SECRET_PARAM + "=" + secret + "&" + Constants.STATUS_PARAM + "=3";
                var json = client.MakeRequest();
                Console.WriteLine(json);

                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(ResponseObject));
                object objResponse = jsonSerializer.ReadObject(CommonUtilities.convertStringToStream(json));
                jsonResponse = objResponse as ResponseObject;
                ProcessResponse(jsonResponse);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            WrapperResponse res = new WrapperResponse();
            if (jsonResponse != null)
            {
                res.errorCode = jsonResponse.ErrorCode;
                res.errorMessage = jsonResponse.ErrorMessage;
                res.result = "Peripheral updated !!!";
            }

            return res;
        }

        private void ProcessResponse(ResponseObject locationsResponse)
        {
            Console.WriteLine("error code: " + locationsResponse.ErrorCode);
            Console.WriteLine("error message: " + locationsResponse.ErrorMessage);
        }
        
    }
}
