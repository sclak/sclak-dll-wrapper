﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace com.sclak.sclak
{
    [Guid("58814D5D-015C-43a0-9031-A2D7B47ECBAC")]
    public interface ISclakWrapper
    {
        [DispId(1)]
        WrapperResponse getToken(string user, string password);
        [DispId(2)]
        WrapperResponse getAvailableBtCode(string token, string typeId, string productsLotTag);
        [DispId(3)]
        WrapperResponse updatePeripheral(string token, string btcode, string sequence, string initialCode, string romId, string fwVersion);
        [DispId(4)]
        WrapperResponse getPeripheralIdWithBtCode(string token, string btcode);
        [DispId(5)]
        WrapperResponse getPeripheralSecretWithBtCode(string token, string btcode);
		[DispId(6)]
		WrapperResponse updatePeripheralVersion(string token, string btcode, string fwVersion);
    }
}