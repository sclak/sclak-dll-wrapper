﻿using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace com.sclak.sclak
{

    [DataContract]
    public class ResponseObject
    {
        [DataMember(Name = "error_code")]
        public int ErrorCode { get; set; }

        [DataMember(Name = "error_message")]
        public string ErrorMessage { get; set; }
    }

	[DataContract]
    public class TimeResponse : ResponseObject
    {
		[DataMember(Name = "ipaddress")]
        public string IpAddress { get; set; }

		[DataMember(Name = "timezone")]
        public string TimeZone { get; set; }

		[DataMember(Name = "time")]
        public string Time { get; set; }
    }

    [DataContract]
    public class TokenResponse : ResponseObject
    {
        [DataMember(Name = "token")]
        public string Token { get; set; }

    }

    [DataContract]
    public class BtcodeResponse : ResponseObject
    {
        [DataMember(Name = "btcode")]
        public string Btcode { get; set; }

        [DataMember(Name = "status")]
        public int Status { get; set; }

        [DataMember(Name = "id")]
        public string Id { get; set; }

    }

    [DataContract]
    public class PeripheralResponse : ResponseObject
    {
        [DataMember(Name = "btcode")]
        public string Btcode { get; set; }

        [DataMember(Name = "lot")]
        public string Lot { get; set; }

        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "desc")]
        public string Desciption { get; set; }

        [DataMember(Name = "address")]
        public string Address { get; set; }

        [DataMember(Name = "longitude")]
        public string Longitude { get; set; }

        [DataMember(Name = "latitude")]
        public string Latitude { get; set; }

        [DataMember(Name = "enabled")]
        public string Enabled { get; set; }

        [DataMember(Name = "insert_time")]
        public string InsertTime { get; set; }

        [DataMember(Name = "secret_code")]
        public string SecretCode { get; set; }        
    }

    [DataContract]
    public class SetSecretCodeRequest
    {
        [DataMember(Name = "sequence")]
        public string Sequence { get; set; }

        [DataMember(Name = "initial_code")]
        public string InitialCode { get; set; }

        [DataMember(Name = "rom_id")]
        public string RomId { get; set; }        
    }
}
