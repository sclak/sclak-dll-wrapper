﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.sclak.sclak
{
    public class Constants
    {
		// Debug Mode
		public const bool DEBUG = false;
        // version
        public const string VERSION = "1.0.5";
		// Base Urls
		// Development
		// public const string BASE_URL = @"https://sclakcollaudo.ngrok.io/";
		// Integration
        // public const string BASE_URL = @"http://sclakcollaudo.ngrok.io/";
		// Test	
        //public const string BASE_URL = @"https://api-test.sclak.com/";
		// Prod
		public const string BASE_URL = @"https://api.sclak.com/";
		// Prod
        //public const string BASE_URL = @"https://api-pve.sclak.ovh/";
		// Other Consts
		public const string TIME_URL = "time";
        public const string TOKEN_URL = "auth_tokens";
        public const string BTCODE_URL = "peripherals/available_btcode";
        public const string PERIPHERALS_URL = "peripherals/";
        public const string SET_SECRET_PREFIX_URL = "/set_secret_code";

        public const string EMAIL_PARAM = "email";
        public const string FW_CODE_PARAM = "fw_code";
		public const string PRODUCT_LOT_TAG_PARAM = "products_lot_tag";
        public const string PASSWORD_PARAM = "password";
        public const string ID_PARAM = "id";
        public const string SECRET_PARAM = "secret_code";
        public const string STATUS_PARAM = "status";
        public const string SEQUENCE_PARAM = "sequence";
        public const string INITIAL_CODE_PARAM = "initial_code";
        public const string ROM_ID_PARAM = "rom_id";
		public const string FW_VERSION_CODE_PARAM = "peripheral_firmware_version";
    }
}