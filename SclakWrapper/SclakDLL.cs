﻿using System;
using System.IO;
using System.Collections;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.InteropServices;

namespace com.sclak.sclak
{
    [Guid("37468844-6072-4cd3-A213-C394E47E5FDA"),
    ClassInterface(ClassInterfaceType.None),
    ComSourceInterfaces(typeof(ISclakWrapper))]
    public class SclakDLL : ISclakWrapper
    {

        public SclakDLL() { }

		public WrapperResponse getTime()
		{
			TimeResponse jsonResponse = null;
            WrapperResponse res = new WrapperResponse();

            try
            {
                var client = new RestClient();
				client.EndPoint = Constants.BASE_URL + Constants.TIME_URL;
                client.Method = HttpVerb.GET;

                NameValueCollection parms = new NameValueCollection();
                var json = client.MakeRequest(CommonUtilities.toQueryString(parms));

				DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(TimeResponse));
                object objResponse = jsonSerializer.ReadObject(CommonUtilities.convertStringToStream(json));
				jsonResponse = objResponse as TimeResponse;
                ProcessResponse(jsonResponse);
            }
            catch (Exception e)
            {
                res.errorCode = 100;
                res.errorMessage = e.Message;
            }

            if (jsonResponse != null)
            {
                res.errorCode = jsonResponse.ErrorCode;
                res.errorMessage = jsonResponse.ErrorMessage;
                res.result = jsonResponse.Time;
            }

            return res;
		}

        public WrapperResponse getToken(string user, string password)
        {
            TokenResponse jsonResponse = null;
            WrapperResponse res = new WrapperResponse();

            try
            {
                var client = new RestClient();
                client.EndPoint = Constants.BASE_URL + Constants.TOKEN_URL;
                client.Method = HttpVerb.GET;

                NameValueCollection parms = new NameValueCollection();
                parms.Add(Constants.EMAIL_PARAM, user);
                parms.Add(Constants.PASSWORD_PARAM, password);
                var json = client.MakeRequest(CommonUtilities.toQueryString(parms));
                
                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(TokenResponse));
                object objResponse = jsonSerializer.ReadObject(CommonUtilities.convertStringToStream(json));
                jsonResponse = objResponse as TokenResponse;
                ProcessResponse(jsonResponse);
            }
            catch (Exception e)
            {
                res.errorCode = 100;
                res.errorMessage = e.Message;
            }

            if (jsonResponse != null)
            {
                res.errorCode = jsonResponse.ErrorCode;
                res.errorMessage = jsonResponse.ErrorMessage;
                res.result = jsonResponse.Token;
            }

            return res;
        }

        public WrapperResponse getPeripheralIdWithBtCode(string token, string btcode)
        {
            PeripheralResponse jsonResponse = getPeripheralWithBtCode(token, btcode);

            WrapperResponse res = new WrapperResponse();
            if (jsonResponse != null)
            {
                res.errorCode = jsonResponse.ErrorCode;
                res.errorMessage = jsonResponse.ErrorMessage;
                res.result = jsonResponse.Id;
            }
            else
            {
                res.errorCode = 100;
                res.errorMessage = "cannot getPeripheralWithBtCode";
            }

            return res;
        }

        public WrapperResponse getPeripheralSecretWithBtCode(string token, string btcode)
        {
            PeripheralResponse jsonResponse = getPeripheralWithBtCode(token, btcode);

            WrapperResponse res = new WrapperResponse();
            if (jsonResponse != null)
            {
                res.errorCode = jsonResponse.ErrorCode;
                res.errorMessage = jsonResponse.ErrorMessage;
                res.result = jsonResponse.SecretCode;
            }
            else
            {
                res.errorCode = 100;
                res.errorMessage = "cannot getPeripheralWithBtCode";
            }

            return res;
        }

        private PeripheralResponse getPeripheralWithBtCode(string token, string btcode)
        {
            PeripheralResponse jsonResponse = null;

            try
            {
                var client = new RestClient();
                client.EndPoint = Constants.BASE_URL + Constants.PERIPHERALS_URL + btcode;
                client.Method = HttpVerb.GET;
                client.AuthHeader = token;

                var json = client.MakeRequest();

                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(PeripheralResponse));
                object objResponse = jsonSerializer.ReadObject(CommonUtilities.convertStringToStream(json));
                jsonResponse = objResponse as PeripheralResponse;
                ProcessResponse(jsonResponse);

                // exception used to test the catch
                //throw new System.ArgumentException("Parameter cannot be null", "original");

            } catch (Exception ex) {

                jsonResponse = new PeripheralResponse();
                jsonResponse.ErrorCode = -1000;
                jsonResponse.ErrorMessage = ex.ToString();
            }

            return jsonResponse;
        }

        public WrapperResponse getAvailableBtCode(string token, string typeId, string productLotTag)
        {
            BtcodeResponse jsonResponse = null;
            WrapperResponse res = new WrapperResponse();

            try
            {
                var client = new RestClient();
                client.EndPoint = Constants.BASE_URL + Constants.BTCODE_URL;
                client.Method = HttpVerb.GET;
                client.AuthHeader = token;

                NameValueCollection parms = new NameValueCollection();
                parms.Add(Constants.FW_CODE_PARAM, typeId);
				parms.Add(Constants.PRODUCT_LOT_TAG_PARAM, productLotTag);
                var json = client.MakeRequest(CommonUtilities.toQueryString(parms));
                
                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(BtcodeResponse));
                object objResponse = jsonSerializer.ReadObject(CommonUtilities.convertStringToStream(json));
                jsonResponse = objResponse as BtcodeResponse;
                ProcessResponse(jsonResponse);
            }
            catch (Exception e)
            {
                res.errorCode = 100;
                res.errorMessage = e.Message;
            }

            if (jsonResponse != null)
            {
                res.errorCode = jsonResponse.ErrorCode;
                res.errorMessage = jsonResponse.ErrorMessage;
                res.result = jsonResponse.Btcode;
            }

            return res;
        }

        public WrapperResponse updatePeripheral(string token, string btcode, string sequence, string initialCode, string romId, string fwVersion)
        {
            ResponseObject jsonResponse = null;
            WrapperResponse res = new WrapperResponse();

            try
            {
                var client = new RestClient();
                client.EndPoint = Constants.BASE_URL + Constants.PERIPHERALS_URL + btcode + Constants.SET_SECRET_PREFIX_URL;
                client.Method = HttpVerb.PUT;
                client.AuthHeader = token;

                //Console.WriteLine("sequence: " + sequence);
                //Console.WriteLine("initialCode: " + initialCode);
                //Console.WriteLine("romId: " + romId);

                // create model to be converted to request data
                SetSecretCodeRequest requestData = new SetSecretCodeRequest();
                requestData.Sequence = sequence;
                requestData.InitialCode = initialCode;
                requestData.RomId = romId;

                // convert model to JSON
                DataContractJsonSerializer jsonWriter = new DataContractJsonSerializer(typeof(SetSecretCodeRequest));
                var stream = new MemoryStream();
                jsonWriter.WriteObject(stream, requestData);
                var postString = CommonUtilities.convertStreamToString(stream);
                //Console.WriteLine("JSON: " + postString);

                /*client.PostData = Constants.SEQUENCE_PARAM + "=" + sequence + "&" + 
					Constants.INITIAL_CODE_PARAM + "=" + initialCode + "&" + 
						Constants.ROM_ID_PARAM + "=" + romId;
				if (null != fwVersion) {
					client.PostData += "&" + Constants.FW_VERSION_CODE_PARAM + "=" + fwVersion;
				}*/

                client.PostData = postString;

                var json = client.MakeRequest();
                
                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(ResponseObject));
                object objResponse = jsonSerializer.ReadObject(CommonUtilities.convertStringToStream(json));
                jsonResponse = objResponse as ResponseObject;
                ProcessResponse(jsonResponse);
            }
            catch (Exception ex)
            {
                res.errorCode = 100;
                res.errorMessage = ex.ToString();
            }

            if (null != jsonResponse)
            {
				res.errorCode = jsonResponse.ErrorCode;
				res.errorMessage = jsonResponse.ErrorMessage;

                if (res.errorCode == 0)
                {
                    //get peripheral to have the ID
                    PeripheralResponse peripheralResponse = getPeripheralWithBtCode(token, btcode);

                    res = new WrapperResponse();
                    if (peripheralResponse == null)
                    {
                        res.errorCode = 100;
                        res.errorMessage = "cannot getPeripheralWithBtCode";
                        return res;
                    }
                }              
            }

            return res;
        }

		public WrapperResponse updatePeripheralVersion(string token, string btcode, string fwVersion)
		{
			PeripheralResponse pResponse = null;
			WrapperResponse res = null;
			
			try
			{
				var client = new RestClient();
				client.EndPoint = Constants.BASE_URL + Constants.PERIPHERALS_URL + btcode;
				client.Method = HttpVerb.GET;
				client.AuthHeader = token;
				
				var json = client.MakeRequest();
				
				DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(PeripheralResponse));
				object objResponse = jsonSerializer.ReadObject(CommonUtilities.convertStringToStream(json));
				pResponse = objResponse as PeripheralResponse;

                var peripheralId = "-1";
				ResponseObject jsonResponse = null;
				res = new WrapperResponse();
				
				try
				{
                    peripheralId = pResponse.Id;

					client = new RestClient();
                    client.EndPoint = Constants.BASE_URL + Constants.PERIPHERALS_URL + peripheralId;
					client.Method = HttpVerb.PUT;
					client.AuthHeader = token;
					
					client.PostData = "{\"" + Constants.FW_VERSION_CODE_PARAM + "\": \"" + fwVersion + "\"}";
					
					json = client.MakeRequest();
					
					jsonSerializer = new DataContractJsonSerializer(typeof(ResponseObject));
					objResponse = jsonSerializer.ReadObject(CommonUtilities.convertStringToStream(json));
					jsonResponse = objResponse as ResponseObject;

					if (null != jsonResponse)
					{
						res.errorCode = jsonResponse.ErrorCode;
						res.errorMessage = jsonResponse.ErrorMessage;

                        if (res.errorCode == 0)
                        {
                            // change peripheral status
                            res = changePeripheralStatus(token, peripheralId);
                            if (res.errorCode == 0)
                            {
                                res.result = "Peripheral correctly updated";
                            }
                        }
					}
					return res;
				}
				catch (Exception e)
				{
					res.errorCode = 100;
					res.errorMessage = e.Message;
				}
				
				if (null != jsonResponse)
				{
					res.errorCode = jsonResponse.ErrorCode;
					res.errorMessage = jsonResponse.ErrorMessage;
				}
				return res;

			} catch (Exception ex) 
			{
				res.errorCode = 100;
				res.errorMessage = ex.Message;
			}
			
			return res;
		}

        private WrapperResponse changePeripheralStatus(string token, string peripheralId)
        {
            ResponseObject jsonResponse = null;
            WrapperResponse res = new WrapperResponse();

            try
            {
                var client = new RestClient();
                client.EndPoint = Constants.BASE_URL + Constants.PERIPHERALS_URL + peripheralId;
                client.Method = HttpVerb.PUT;
                client.AuthHeader = token;

                client.PostData = "{\"" + Constants.STATUS_PARAM + "\": \"3\"}";
                var json = client.MakeRequest();

                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(ResponseObject));
                object objResponse = jsonSerializer.ReadObject(CommonUtilities.convertStringToStream(json));
                jsonResponse = objResponse as ResponseObject;
                ProcessResponse(jsonResponse);
            }
            catch (Exception e)
            {
                res.errorCode = 100;
                res.errorMessage = e.Message;
            }

            if (jsonResponse != null)
            {
                res.errorCode = jsonResponse.ErrorCode;
                res.errorMessage = jsonResponse.ErrorMessage;
                res.result = "Status changed";
            }

            return res;
        }

        private void ProcessResponse(ResponseObject locationsResponse)
        {
            //Console.WriteLine("error code: " + locationsResponse.ErrorCode);
            //Console.WriteLine("error message: " + locationsResponse.ErrorMessage);
        }
    }
}
