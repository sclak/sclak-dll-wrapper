﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.sclak.sclak
{
    public class WrapperResponse
    {
        public WrapperResponse() { }

        public WrapperResponse(int code, string message, string result)
        {
            this.errorCode = code;
            this.errorMessage = message;
            this.result = result;
        }

        public int errorCode;
        public string errorMessage;
        public string result;
    }
}
