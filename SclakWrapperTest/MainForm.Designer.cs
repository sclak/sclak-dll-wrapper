﻿namespace SclakWrapperTest
{
    partial class MainForm
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.userTb = new System.Windows.Forms.TextBox();
            this.passwordTb = new System.Windows.Forms.TextBox();
            this.getTokenBtn = new System.Windows.Forms.Button();
            this.btcodeTb = new System.Windows.Forms.TextBox();
            this.getBtcodeBtn = new System.Windows.Forms.Button();
            this.lotUd = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.initialCodeTb = new System.Windows.Forms.TextBox();
            this.sequenceTb = new System.Windows.Forms.TextBox();
            this.romIdTb = new System.Windows.Forms.TextBox();
            this.updatePeripheralBtn = new System.Windows.Forms.Button();
            this.aesTb = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.consoleTb = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lotUd)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // userTb
            // 
            this.userTb.Location = new System.Drawing.Point(12, 15);
            this.userTb.Name = "userTb";
            this.userTb.Size = new System.Drawing.Size(180, 20);
            this.userTb.TabIndex = 2;
            this.userTb.Text = "collaudo@sclak.com";
            // 
            // passwordTb
            // 
            this.passwordTb.Location = new System.Drawing.Point(198, 15);
            this.passwordTb.Name = "passwordTb";
            this.passwordTb.PasswordChar = '*';
            this.passwordTb.Size = new System.Drawing.Size(180, 20);
            this.passwordTb.TabIndex = 3;
            this.passwordTb.Text = "collaudo";
            // 
            // getTokenBtn
            // 
            this.getTokenBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.getTokenBtn.Location = new System.Drawing.Point(539, 12);
            this.getTokenBtn.Name = "getTokenBtn";
            this.getTokenBtn.Size = new System.Drawing.Size(102, 23);
            this.getTokenBtn.TabIndex = 4;
            this.getTokenBtn.Text = "Auth";
            this.getTokenBtn.UseVisualStyleBackColor = true;
            this.getTokenBtn.Click += new System.EventHandler(this.getTokenBtn_Click);
            // 
            // btcodeTb
            // 
            this.btcodeTb.Location = new System.Drawing.Point(12, 44);
            this.btcodeTb.MaxLength = 12;
            this.btcodeTb.Name = "btcodeTb";
            this.btcodeTb.Size = new System.Drawing.Size(136, 20);
            this.btcodeTb.TabIndex = 5;
            // 
            // getBtcodeBtn
            // 
            this.getBtcodeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.getBtcodeBtn.Enabled = false;
            this.getBtcodeBtn.Location = new System.Drawing.Point(539, 41);
            this.getBtcodeBtn.Name = "getBtcodeBtn";
            this.getBtcodeBtn.Size = new System.Drawing.Size(102, 23);
            this.getBtcodeBtn.TabIndex = 6;
            this.getBtcodeBtn.Text = "Get BTcode";
            this.getBtcodeBtn.UseVisualStyleBackColor = true;
            this.getBtcodeBtn.Click += new System.EventHandler(this.getBtcodeBtn_Click);
            // 
            // lotUd
            // 
            this.lotUd.Location = new System.Drawing.Point(198, 43);
            this.lotUd.Name = "lotUd";
            this.lotUd.Size = new System.Drawing.Size(120, 20);
            this.lotUd.TabIndex = 7;
            this.lotUd.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.initialCodeTb);
            this.groupBox1.Controls.Add(this.sequenceTb);
            this.groupBox1.Controls.Add(this.romIdTb);
            this.groupBox1.Location = new System.Drawing.Point(12, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(513, 100);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Peripheral data";
            // 
            // initialCodeTb
            // 
            this.initialCodeTb.Location = new System.Drawing.Point(81, 71);
            this.initialCodeTb.MaxLength = 500;
            this.initialCodeTb.Name = "initialCodeTb";
            this.initialCodeTb.Size = new System.Drawing.Size(426, 20);
            this.initialCodeTb.TabIndex = 8;
            this.initialCodeTb.Text = "0000000000000000";
            // 
            // sequenceTb
            // 
            this.sequenceTb.Location = new System.Drawing.Point(81, 45);
            this.sequenceTb.MaxLength = 500;
            this.sequenceTb.Name = "sequenceTb";
            this.sequenceTb.Size = new System.Drawing.Size(426, 20);
            this.sequenceTb.TabIndex = 7;
            this.sequenceTb.Text = "0";
            // 
            // romIdTb
            // 
            this.romIdTb.Location = new System.Drawing.Point(81, 19);
            this.romIdTb.MaxLength = 500;
            this.romIdTb.Name = "romIdTb";
            this.romIdTb.Size = new System.Drawing.Size(426, 20);
            this.romIdTb.TabIndex = 6;
            this.romIdTb.Text = "49e2780500000005";
            // 
            // updatePeripheralBtn
            // 
            this.updatePeripheralBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.updatePeripheralBtn.Enabled = false;
            this.updatePeripheralBtn.Location = new System.Drawing.Point(539, 141);
            this.updatePeripheralBtn.Name = "updatePeripheralBtn";
            this.updatePeripheralBtn.Size = new System.Drawing.Size(102, 23);
            this.updatePeripheralBtn.TabIndex = 9;
            this.updatePeripheralBtn.Text = "Update Peripheral";
            this.updatePeripheralBtn.UseVisualStyleBackColor = true;
            this.updatePeripheralBtn.Click += new System.EventHandler(this.updatePeripheralBtn_Click);
            // 
            // aesTb
            // 
            this.aesTb.Location = new System.Drawing.Point(6, 19);
            this.aesTb.MaxLength = 12;
            this.aesTb.Name = "aesTb";
            this.aesTb.Size = new System.Drawing.Size(136, 20);
            this.aesTb.TabIndex = 9;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.aesTb);
            this.groupBox2.Location = new System.Drawing.Point(12, 176);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 100);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "AES 128";
            // 
            // consoleTb
            // 
            this.consoleTb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.consoleTb.Location = new System.Drawing.Point(12, 345);
            this.consoleTb.Multiline = true;
            this.consoleTb.Name = "consoleTb";
            this.consoleTb.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.consoleTb.Size = new System.Drawing.Size(629, 131);
            this.consoleTb.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "ROM id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Sequence";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Initial code";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 488);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.updatePeripheralBtn);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lotUd);
            this.Controls.Add(this.getBtcodeBtn);
            this.Controls.Add(this.btcodeTb);
            this.Controls.Add(this.getTokenBtn);
            this.Controls.Add(this.passwordTb);
            this.Controls.Add(this.userTb);
            this.Controls.Add(this.consoleTb);
            this.Name = "MainForm";
            this.Text = "SCLAK Wrapper";
            ((System.ComponentModel.ISupportInitialize)(this.lotUd)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox userTb;
        private System.Windows.Forms.TextBox passwordTb;
        private System.Windows.Forms.Button getTokenBtn;
        private System.Windows.Forms.TextBox btcodeTb;
        private System.Windows.Forms.Button getBtcodeBtn;
        private System.Windows.Forms.NumericUpDown lotUd;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox initialCodeTb;
        private System.Windows.Forms.TextBox sequenceTb;
        private System.Windows.Forms.TextBox romIdTb;
        private System.Windows.Forms.Button updatePeripheralBtn;
        private System.Windows.Forms.TextBox aesTb;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox consoleTb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
    }
}

