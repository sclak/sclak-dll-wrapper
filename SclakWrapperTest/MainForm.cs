﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using com.sclak.sclak;

namespace SclakWrapperTest
{
    public partial class MainForm : Form
    {
        private SclakDLL sclakDLL = new SclakDLL();
        private string currToken, currBtcode;

        public MainForm()
        {
            InitializeComponent();
        }

        private void getTokenBtn_Click(object sender, EventArgs e)
        {
            currToken = "";
            WrapperResponse res = sclakDLL.getToken(userTb.Text, passwordTb.Text);
            if (res.errorCode == 0) {
                currToken = res.result;
                getBtcodeBtn.Enabled = currToken.Length != 0;
            }
            
            writeConsoleMessage("getToken", res);
        }

        private void getBtcodeBtn_Click(object sender, EventArgs e)
        {
            currBtcode = "";
            WrapperResponse res = sclakDLL.getAvailableBtCode(currToken, lotUd.Value.ToString());
            if (res.errorCode == 0) {
                currBtcode = res.result;
                btcodeTb.Text = currBtcode;
                updatePeripheralBtn.Enabled = currBtcode.Length != 0;
            }
            
            writeConsoleMessage("getAvailableBtCode", res);
        }

        private void updatePeripheralBtn_Click(object sender, EventArgs e)
        {
            //string secret = ridTb.Text + secretTb.Text + romIdTb.Text;
            WrapperResponse res = sclakDLL.updatePeripheral(currToken, currBtcode, sequenceTb.Text, initialCodeTb.Text, romIdTb.Text);
            writeConsoleMessage("updatePeripheral", res);
        }

        private void writeConsoleMessage(string methodName, WrapperResponse res)
        {
            string message;
            if (res.errorCode != 0)
            {
                message = "err code: " + res.errorCode + ", err message: " + res.errorMessage;
            }
            else
            {
                message = res.result;
            }

            consoleTb.Text += DateTime.Now.ToString() + " | " + methodName + " | " + message + "\r\n";
            consoleTb.ScrollToCaret();
        }
    }
}
